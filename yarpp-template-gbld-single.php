<?php
/*
YARPP Template: GBLD - Single
Author: ThachPham
Description: Bài viết liên quan trong trang single
*/
?>

<div class="related-posts">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="post">
            <div class="left fill">
                <?php the_post_thumbnail('thumb-small'); ?>
            </div>
            <div class="right">
                <a href="<?php the_permalink(); ?>" class="entry-title"><?php the_title(); ?></a>
                <div class="entry-content"><?php the_excerpt(); ?></div>
            </div>
        </div>

    <?php endwhile; endif; ?>
</div>
