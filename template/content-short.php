<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf small-thumb' ); ?> role="article">
	<section class="left entry-left">
		<div class="entry-thumbnail"><?php the_post_thumbnail( 'thumb-small' ); ?></div>
		<div class="social-btn">

		</div>
	</section>
	<section class="right entry-right">
		<header class="article-header">

			<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
		</header>

		<section class="entry-content cf">
			<?php the_excerpt(); ?>
		</section>
		<footer class="entry-footer">
			<p class="byline entry-meta vcard">
				<?php printf( __( 'Đăng ngày', 'bonestheme' ).' %1$s %2$s',
						/* the time the post was published */
						'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
						/* the author of the post */
						'<span class="by">'.__( 'bởi', 'bonestheme').'</span> <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
				); ?>
			</p>
		</footer>
	</section>

</article>