<?php get_header(); global $gbld_options; ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
						<div class="text_center ads ads-top">
							<?php echo $gbld_options['ads-top-post']; ?>
						</div>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php
							/*
                             * This is the default post format.
                             *
                             * So basically this is a regular post. if you don't want to use post formats,
                             * you can just copy ths stuff in here and replace the post format thing in
                             * single.php.
                             *
                             * The other formats are SUPER basic so you can style them as you like.
                             *
                             * Again, If you want to remove post formats, just delete the post-formats
                             * folder and replace the function below with the contents of the "format.php" file.
                            */
							?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

								<header class="article-header entry-header">

									<h1 class="entry-title single-title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>

									<section class="entry-meta">

										<section class="byline-left">
											<p class="byline entry-meta vcard">

												<?php printf( __( 'Đăng ngày', 'bonestheme' ).' %1$s %2$s',
														/* the time the post was published */
														'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
														/* the author of the post */
														'<span class="by">'.__( 'bởi', 'bonestheme' ).'</span> <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
												); ?>

											</p>
											<span class="byline"><?php printf( __( 'Chuyên mục', 'bonestheme' ).': %1$s', get_the_category_list(', ') ); ?></span>
										</section>
										<section class="byline-right"><div class="fb-like" data-width="350" data-href="https://www.facebook.com/giupbanlamdepfanpage/?ref=bookmarks" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div></section>

									</section>
								</header> <?php // end article header ?>
								<?php
									if (has_tag('chi-em-chia-se')) :
								?>
								<div class="guest-post"><strong>Lưu ý</strong>: Bài viết bạn đang xem là thông tin được thành viên tham gia Blog Giúp Bạn Làm Đẹp chia sẻ lại. Không phải là bài viết của Oanh nhé.</div>
								<?php endif; ?>
								<section class="entry-content cf" itemprop="articleBody">
									<?php
									// the content (pretty self explanatory huh)
									the_content();

									/*
                                     * Link Pages is used in case you have posts that are set to break into
                                     * multiple pages. You can remove this if you don't plan on doing that.
                                     *
                                     * Also, breaking content up into multiple pages is a horrible experience,
                                     * so don't do it. While there are SOME edge cases where this is useful, it's
                                     * mostly used for people to get more ad views. It's up to you but if you want
                                     * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
                                     *
                                     * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
                                     *
                                    */
									wp_link_pages( array(
											'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
											'after'       => '</div>',
											'link_before' => '<span>',
											'link_after'  => '</span>',
									) );
									?>
									<?php related_posts() ?>
									<section class="entry-additional">
										<div class="left">
											<div class="toppost">
												<h4>Bài viết hay nhất</h4>
												<ul style="list-style-type: none; margin: 0">
												<?php
													$haynhat = new WP_Query( 'tag=hay-nhat&posts_per_page=4&orderby=rand' );
													if ($haynhat->have_posts()) : $i = 0; while ($haynhat->have_posts()) : $haynhat->the_post(); $i++;
												?>

															<li>
																<?php echo '#'.$i; ?>. <a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
															</li>

												<?php endwhile; endif; ?>
												</ul>
											</div>
										</div>
										<div class="right">
											<div class="share-btn">
												<h4>Chia sẻ cùng bạn bè</h4>
												<?php dd_fblike_generate('Like Box Count') ?> <?php dd_google1_generate('Normal') ?> <?php dd_twitter_generate('Normal','twitter_username') ?>
												<p><a class="dcma ext-link" href="http://www.dmca.com/Protection/Status.aspx?ID=b54c5d99-2e5a-4ad3-9437-d94366e6b697" title="DMCA" rel="external nofollow" data-wpel-target="_blank" spfieldtype="null" spsourceindex="39"> <img src="http://images.dmca.com/Badges/dmca_protected_16_120.png?ID=b54c5d99-2e5a-4ad3-9437-d94366e6b697" alt="DMCA.com" spfieldtype="null" spsourceindex="40"></a></p>
											</div>
										</div>
									</section>
									<div class="text_center ads ads-bottom">
										<?php echo $gbld_options['ads-bottom-post']; ?>
									</div>
									<?php // echo do_shortcode('[starbox]'); ?>


								</section> <?php // end article section ?>

								<?php echo do_shortcode('[related_posts_by_tax before_title="<h4>" after_title="</h4>" taxonomies="category,post_tag" posts_per_page="8" title="Bài liên quan"]'); ?>

								<?php //comments_template(); ?>

							</article> <?php // end article ?>

						<?php endwhile; ?>

						<?php else : ?>

							<article id="post-not-found" class="hentry cf">
									<header class="article-header">
										<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
									</footer>
							</article>

						<?php endif; ?>

					</main>

					<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
